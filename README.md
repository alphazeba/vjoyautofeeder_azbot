azBot.

this is a bot the provides an interface between an IRC channel and shaul_ei's vJoy application (https://sourceforge.net/projects/vjoystick/).

Bot commands
  arguments are seperated from commands with ~(tilda)
chat: send a message to the IRC channel as the bot.
join: join the specified channel or the last channel left if no argument is provided
leave: leave the current channel
quit: shutdown azBot
cmd: send a command to the vJoy feeder
   cmd~tap: change to manual tap mode (buttons are released after a moment)
   cmd~manual: change to manual hold mode (buttons are held until release is sent)
   cmd~random: change to random input mode. (buttons are pressed at random)
no command: these arguments will be sent straight to the vJoy feeder as if they came from the IRC chat. these are button commands. up,down,left,right,select,start,a,b,release.