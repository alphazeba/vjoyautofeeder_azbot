﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//
using AronVJoyFeeder;
using AronKeyboardInterface;
using AronIrcInterface;

namespace vJoyAutoFeeder
{
    class Program
    {
        static void Main(string[] args)
        {
            string defaultIrc = "chat.freenode.net";
            string defaultBotName = "azbot";
            string defaultPassword = "";

            bool running = true;
            while (running)
            {

                //get some info.
                Console.WriteLine("Welcome to Aron's vJoyAutoFeeder");
                Console.Write("irc site: ");
                string irc = Console.ReadLine();
                Console.Write("bot name: ");
                string botName = Console.ReadLine();
                Console.Write("Password: ");
                string password = Console.ReadLine();


                //check for default login.
                if (irc == "d")
                {
                    irc = defaultIrc;
                }
                if (botName == "d")
                {
                    botName = defaultBotName;
                }
                if (password == "d")
                {
                    password = defaultPassword;
                }

                //the actual session

                Console.WriteLine("beginning session");
                session(irc, botName, password);
                Console.WriteLine("the session has ended");

                //continue with another session?
                Console.WriteLine("Log back in? (y/n)");
                if(Console.ReadKey().KeyChar != 'y')
                {
                    running = false;
                }
            }
        }

        static void session(string ircSite , string botName , string password)
        {
            //prep main loop.
            bool running = true;

            //get the emulator feeder portion of the program ready.
            VJoyFeeder feeder = new VJoyFeeder(1);
            running = feeder.initialize();

            //prepare for keyboard input.
            KeyboardInterface input = new KeyboardInterface();
            //keyboard interface begins its own loop within itself.

            //get the irc bot ready.
            IrcInterface bot = new IrcInterface();
            // bot.initialize("irc.chat.twitch.tv", 6667, "alphazeba", "#alphazeba", "oauth:password");
            bot.initialize(ircSite, 6667, botName, "azazpokebot",password);

            bot.connect();


            while (running)
            {

                if (bot.hasOutput())
                {
                    string s = bot.getOutput();
                    feeder.passInput(s);

                }


                if (input.checkOutput())
                {
                    String s = input.getOutput();
                    string[] split = s.Split('`');//currently splits input at the '`'

                    switch (split[0]) // this switch checks keyboard commands for keywords before sending it into the feeder application.
                    {
                        case "exit":
                            running = false;
                            break;
                        case "join":
                            if (split.Length > 1)
                                bot.joinRoom(split[1]);
                            else
                                bot.joinRoom();
                            break;
                        case "part":
                            bot.partRoom();
                            break;
                        case "chat":
                            if(split.Length > 1)
                                bot.chat(split[1]);
                            break;
                        case "pure":
                            if (split.Length > 1)
                                bot.pure(split[1]);
                            break;
                        case "cmd":
                            if (split.Length > 1)
                                feeder.passCommand(split[1]);
                            break;
                        default:
                            feeder.passInput(s);
                            break;
                    }
                }

                //how to leave a channel
                //PART #channel

                //this updates the controller buttons that are being pressed.
                feeder.update();

                //it is important to sleep a bit between doing things.
                //this is not an intensive program and even this long of a wait is entirely unnoticeable.
                System.Threading.Thread.Sleep(50);

            }
        }
    }
}
