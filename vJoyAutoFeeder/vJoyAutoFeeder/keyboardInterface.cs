﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AronKeyboardInterface
{
    class KeyboardInterface
    {
        #region private variables
        private String input;
        private String output;

        private bool hasOutput;

        private bool listen; //whether or not to listen for keyboard input.

        private delegate void AsyncBeginListeningCaller();
        AsyncBeginListeningCaller caller;

        #endregion

        public KeyboardInterface()
        {
            initialize();
        }

        public KeyboardInterface(bool b)
        {
            initialize(b);
        }

        private void initialize()
        {
            input = "";
            output = "";
            hasOutput = false;
            listen = true;

            caller = new AsyncBeginListeningCaller(beginListening);
            caller.BeginInvoke(null,null);
        }
        private void initialize(bool b)
        {
            initialize();
            listen = b;
        }

        

        #region private methods
        private void sendOutput()
        {
            output = input;
            input = "";
            hasOutput = true;
        }
        #endregion

        public void beginListening()
        {
            while (true)
            {
                ConsoleKeyInfo c = Console.ReadKey(true);
                if (input.Length == 0)
                {
                    Console.Write('>');
                }

                Console.Write(c.KeyChar);
                if (c.Key == ConsoleKey.Enter)
                {
                    Console.WriteLine();
                    sendOutput();
                }
                else if (c.Key == ConsoleKey.Backspace)
                {
                    if (input.Length > 0)
                    {
                        input = input.Remove(input.Length - 1, 1);
                        Console.WriteLine();
                        Console.Write('>' + peak());
                    }

                }
                else
                {
                    input += c.KeyChar;
                }
            }
        }

        //update is deprecated.
        //it is much better to use the asynchronous begin listening delegate solution.
        public void update()
        {
            if (listen)
            {
                while (Console.KeyAvailable)
                {
                    ConsoleKeyInfo c = Console.ReadKey();
                    if (c.Key == ConsoleKey.Enter)
                    {
                        Console.WriteLine();
                        sendOutput();
                    }
                    else if (c.Key == ConsoleKey.Backspace)
                    {
                        if (input.Length > 0)
                        {
                            input = input.Remove(input.Length - 1, 1);
                            Console.WriteLine();
                            Console.Write(peak());
                        }

                    }
                    else
                    {
                        input += c.KeyChar;
                    }
                }
            }
           
        }

        public bool checkOutput()
        {
            return hasOutput;
        }

        public void passOutput(String s)
        {
            input = s;
            sendOutput();
        }

        public String getOutput()
        {
            hasOutput = false;
            return output;
        }

        public String peak()
        {
            return input;
        }
    }
}
