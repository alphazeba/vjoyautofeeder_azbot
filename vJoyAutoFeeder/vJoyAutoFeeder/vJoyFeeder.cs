﻿using System;
using System.Collections.Generic;

using vJoyInterfaceWrap;

namespace AronVJoyFeeder
{
    public enum Button { a, b, select, start, left, right, up, down, length };

    public class VJoyFeeder
    {
        //varibales
        #region private variables
        private vJoy joystick;
        private vJoy.JoystickState iReport;
        private uint id;
        private int maxval;

        private enum Mode { manualWait, manualTap, random }
        private Mode mode;

        private static Queue<Button> toPress;

        private static bool[] pressed;

        private Random rand;

        private String input;
        private bool newInput;

        private string command;
        private bool newCommand;
        #endregion


        //functions 

        public VJoyFeeder(uint id)
        {
            this.id = id;
            toPress = new Queue<Button>();
            pressed = new bool[(int)Button.length];

            // Create one joystick object and a position structure.
            joystick = new vJoy();
            iReport = new vJoy.JoystickState();

            rand = new Random();

            mode = Mode.manualTap;
        }

        ~VJoyFeeder()
        {
            joystick.RelinquishVJD(id);
        }

        public bool initialize()
        {
            if (id <= 0 || id > 16)
            {
                Console.WriteLine("Illegal device ID {0}\nExit!", id);

                return false;
            }

            // Get the driver attributes (Vendor ID, Product ID, Version Number)
            if (!joystick.vJoyEnabled())
            {
                Console.WriteLine("vJoy driver not enabled: Failed Getting vJoy attributes.\n");

                return false;
            }
            else
                Console.WriteLine("Vendor: {0}\nProduct :{1}\nVersion Number:{2}\n", joystick.GetvJoyManufacturerString(), joystick.GetvJoyProductString(), joystick.GetvJoySerialNumberString());

            // Get the state of the requested device
            VjdStat status = joystick.GetVJDStatus(id);
            switch (status)
            {
                case VjdStat.VJD_STAT_OWN:
                    Console.WriteLine("vJoy Device {0} is already owned by this feeder\n", id);
                    break;
                case VjdStat.VJD_STAT_FREE:
                    Console.WriteLine("vJoy Device {0} is free\n", id);
                    break;
                case VjdStat.VJD_STAT_BUSY:
                    Console.WriteLine("vJoy Device {0} is already owned by another feeder\nCannot continue\n", id);
                    return false;

                case VjdStat.VJD_STAT_MISS:
                    Console.WriteLine("vJoy Device {0} is not installed or disabled\nCannot continue\n", id);
                    return false;
                default:
                    Console.WriteLine("vJoy Device {0} general error\nCannot continue\n", id);
                    return false;
            };

            // Check which axes are supported
            bool AxisX = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_X);
            bool AxisY = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_Y);

            /* //don't need to use these axes.
            bool AxisZ = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_Z);
            
            bool AxisRX = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_RX);
            bool AxisRZ = joystick.GetVJDAxisExist(id, HID_USAGES.HID_USAGE_RZ);
            */
            // Get the number of buttons and POV Hat switchessupported by this vJoy device
            int nButtons = joystick.GetVJDButtonNumber(id);
            //int ContPovNumber = joystick.GetVJDContPovNumber(id);
            //int DiscPovNumber = joystick.GetVJDDiscPovNumber(id);

            // Print results
            Console.WriteLine("\nvJoy Device {0} capabilities:\n", id);
            Console.WriteLine("Number of buttons\t\t{0}\n", nButtons);
            Console.WriteLine("Axis X\t\t{0}\n", AxisX ? "Yes" : "No");
            Console.WriteLine("Axis Y\t\t{0}\n", AxisX ? "Yes" : "No");

            if(!AxisX || !AxisY || nButtons < 4) //if the vjoy is missing an axis or has too few buttons.
            {
                Console.WriteLine("this vJoy device does not have the adequate features.");
                return false;
            }

            // Test if DLL matches the driver
            UInt32 DllVer = 0, DrvVer = 0;
            bool match = joystick.DriverMatch(ref DllVer, ref DrvVer);
            if (match)
                Console.WriteLine("Version of Driver Matches DLL Version ({0:X})\n", DllVer);
            else
                Console.WriteLine("Version of Driver ({0:X}) does NOT match DLL Version ({1:X})\n", DrvVer, DllVer);


            // Acquire the target
            if ((status == VjdStat.VJD_STAT_OWN) || ((status == VjdStat.VJD_STAT_FREE) && (!joystick.AcquireVJD(id))))
            {
                Console.WriteLine("Failed to acquire vJoy device number {0}.\n", id);
                return false;
            }
            else
                Console.WriteLine("Acquired: vJoy device number {0}.\n", id);

            //this will set the maxvalue.
            maxval = prepAxes();

            // Reset this device to default values
            joystick.ResetVJD(id);
            joystick.ResetButtons(id);


            //checks that everything is released by manually releasing them all.
            for (int i = 0; i < (int)Button.length; ++i)
            {

                releaseDir((Button)i);

            }

            return true;
        }

        public void update()
        {
            if (newCommand)
            {
                if (command == "manual")
                {
                    mode = Mode.manualWait;
                    newCommand = false;
                    Console.WriteLine("manual mode");
                }
                else if (command == "random")
                {
                    mode = Mode.random;
                    newCommand = false;
                    Console.WriteLine("random mode");
                }
                else if (command == "tap" || command == "manual tap")
                {
                    mode = Mode.manualTap;
                    newCommand = false;
                    Console.WriteLine("manual tap mode");
                }
            }

            switch (mode)
            {
                case Mode.manualWait:
                    manualInput();
                    break;

                case Mode.random:
                    randomInput();
                    break;
                case Mode.manualTap:
                    manualTap();
                    break;
            }
        }

        public void passInput(String s)
        {
            input = s.ToLower();
            newInput = true;
        }

        public void passCommand(string s)
        {
            command = s.ToLower();
            newCommand = true;
        }

        #region private methods

        private String recieveInput()
        {
            newInput = false;
            return input;
        }

        private string recieveCommand()
        {
            newCommand = false;
            return command;
        }

        private void manualInput()
        {
            if (newInput)
            {
                recieveInput();

                if (input.Equals("a"))
                {
                    pressBut(Button.a);
                }
                else if (input.Equals("b"))
                {
                    pressBut(Button.b);
                }
                else if (input.Equals("select"))
                {
                    pressBut(Button.select);
                }
                else if (input.Equals("start"))
                {
                    pressBut(Button.start);
                }
                else if (input.Equals("left"))
                {
                    pressBut(Button.left);
                }
                else if (input.Equals("right"))
                {
                    pressBut(Button.right);
                }
                else if (input.Equals("up"))
                {
                    pressBut(Button.up);
                }
                else if (input.Equals("down"))
                {
                    pressBut(Button.down);
                }
                else if (input.Equals("release"))
                {
                    releaseAll();
                }
            }
            
        }

        private void manualTap()
        {
            releaseAll();
            manualInput();
        }

        private void randomInput()
        {

            // Feed the device in endless loop
            for (int i = 0; i < 2; ++i)
            {
                Button r = (Button)(rand.Next() % (int)Button.length);
                if ((r != Button.select && r != Button.start) || i == 1)
                { //cannot press select or start for the first button of the 2.
                    toPress.Enqueue(r);
                }
            }


            releaseAll();
            pressButtons(toPress);

          

        }

        private bool pressButtons(Queue<Button> buttons)
        {
            //pushes all the buttons in a queue of buttons.
            bool output = true;
            while (buttons.Count > 0)
            {
                output &= pressBut(buttons.Dequeue());
            }
            return output;
        }

        private bool pressDir(Button but)
        {
            bool output = false;
            switch (but)
            {
                case Button.up:
                    output = joystick.SetAxis(maxval, id, HID_USAGES.HID_USAGE_Y);
                    pressed[(int)Button.down] = false;
                    break;
                case Button.down:
                    output = joystick.SetAxis(0, id, HID_USAGES.HID_USAGE_Y);
                    pressed[(int)Button.up] = false;
                    break;
                case Button.left:
                    output = joystick.SetAxis(0, id, HID_USAGES.HID_USAGE_X);
                    pressed[(int)Button.right] = false;
                    break;
                case Button.right:
                    output = joystick.SetAxis(maxval, id, HID_USAGES.HID_USAGE_X);
                    pressed[(int)Button.left] = false;
                    break;
                default:
                    break;

            }
            return output;
        }

        private bool releaseDir(Button but)
        {
            bool output = false;
            switch (but)
            {
                case Button.up:
                case Button.down:
                    output = joystick.SetAxis(maxval / 2, id, HID_USAGES.HID_USAGE_Y);
                    break;
                case Button.left:
                case Button.right:
                    output = joystick.SetAxis(maxval / 2, id, HID_USAGES.HID_USAGE_X);
                    break;
                default:
                    break;

            }
            return output;
        }

        private bool pressBut(Button but)
        {
            bool output = false;
            switch (but)
            {
                case Button.a:
                case Button.b:
                case Button.select:
                case Button.start:
                    //press the button and check for success.
                    output = joystick.SetBtn(true, id, 1 + (uint)but);
                    //Console.Write("pressing ");
                    //Console.WriteLine(but);
                    break;
                default:
                    output = pressDir(but);
                    break;
            }

            //remember which button is pressed.
            pressed[(int)but] = true;

            //done
            return output;
        }

        private bool releaseBut(Button but)
        {

            bool output = false;
            switch (but)
            {
                case Button.a:
                case Button.b:
                case Button.select:
                case Button.start:
                    //press the button and check for success.
                    output = this.joystick.SetBtn(false, id, 1 + (uint)but);
                    //Console.Write("released button ");
                    //Console.WriteLine(but);
                    break;
                default:
                    output = releaseDir(but);
                    break;
            }


            //remember which button is pressed.
            pressed[(int)but] = false;

            //done
            return output;
        }

        private int prepAxes()
        {
            //uses the X axis as a guide, maxval will be used as the max value for all axes however.
            long val = 0;
            joystick.GetVJDAxisMax(id, HID_USAGES.HID_USAGE_X, ref val);
            return (int)val;
        }

        private void releaseAll()
        {
            for (int i = 0; i < (int)Button.length; ++i)
            {
                if (pressed[i])
                {
                    //Console.Write("attempting to release ");
                    //Console.WriteLine((Button)i);
                    releaseBut((Button)i);
                }
            }
        }

        #endregion
    }

}