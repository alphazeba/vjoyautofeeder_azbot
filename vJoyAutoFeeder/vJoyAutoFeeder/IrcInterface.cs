﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//actually used stuff.
using System.Net.Sockets;
using System.IO;

/*

-this provides a simple irc client.  it recieves input from the host and everyone in the bot's channel.
-provides output to the rest of the program.

-also has the ability to chat in channel.

*/

namespace AronIrcInterface
{
    class IrcInterface
    {
        #region private variables

        private string server;
        private int port;
        private string user;
        private string nick;
        private int mode;
        private string realname;
        private string channel;
        private bool inChannel;
        private string password;

        private TcpClient irc;

        private NetworkStream stream;
        private StreamReader sin;
        private StreamWriter sout;

        private Queue<string> input;
        #endregion

        public bool filter;

        public IrcInterface()
        {
            irc = null;
            stream = null;
            sin = null;
            sout = null;
            input = null;
            filter = true;
        }

        public void initialize(string server,int port,string nick,string channel)
        {
            this.server = server;
            this.port = port;
            this.nick = nick;
            this.channel = channel;
            inChannel = false;
            input = new Queue<string>();

            //this initialize uses default user,realname, and mode
            user = "azbot";
            realname = "Peanut Butter Apple";
            mode = 8;
            password = "";
        }

        public void initialize(string server, int port, string nick, string channel , string password)
        {
            initialize(server, port, nick, channel);
            this.password = password;
        }

        //use this to start off the interface's main loop.
        public async void beginListening()
        {
            while (true)
            {
                if (sin != null)
                {
                    string s = await sin.ReadLineAsync();
                    if (s != null)
                    {


                        if (s != null)
                        {
                            string[] split = s.Split(new char[] { ' ' });

                            if (split[0] == "PING")
                            {
                                sout.WriteLine("PONG " + split[1]);
                                sout.Flush();
                                Console.WriteLine("ponged. :P");
                            }
                            else if(split[1]=="PRIVMSG")
                            {
                                incomingChat(s);
                                //input.Enqueue(s);
                            }
                            else
                            {
                                Console.WriteLine("stream: " + s);
                            }
                        }
                        else
                        {
                            Console.WriteLine("stream:NULL NULL NULL");
                        }
                    }
                }
            }
        }
        
        public bool connect()
        {
            //begin connection attempt.
            Console.WriteLine("bot is attempting to connect");

            //makes a new tcpclient. this will handle connection to the host.
            //after this will just be handling irc protocol to finish the connection.
            irc = new TcpClient(server, port);

            stream = irc.GetStream();

            if(stream == null) //if stream is still null, then something went wrong.
            {
                Console.WriteLine("failed to connect to host");
                return false;
            }

            //build convenient stream input and output.
            sin = new StreamReader(stream);
            sout = new StreamWriter(stream);
            sout.AutoFlush = true;
           
            if(password != "") //if a password was entered, then it should be input first.
            {
                sout.WriteLine("PASS " + password);
            }
            
            //pass your nickname to the host
            sout.WriteLine("NICK " + nick );
            //give the user data.
            //technically this is supposed to be 'user' '(numerical) mode' 'blank' :'realname'
            //but stuff like this isn't checked so you don't really have to.
            sout.WriteLine("USER " + nick + " 8 * :" + nick);

            //flushes stuff out of the output stream that hasn't been sent yet.
            //shouldn't be necessary since autoflush = true.
            sout.Flush();

            this.beginListening();

            return true;
        }

        public void update()
        {
            //this is called during that main cycle.
            //the readin loop is handled in beginListening()
            
        }

        #region getters
        public bool hasOutput()
        {
            return input.Count > 0;
        }

        public string getOutput()
        {
            return input.Dequeue();
        }
        #endregion

        public void chat(string s)
        {
            Console.WriteLine("chatted: " + s);
            sout.WriteLine("PRIVMSG #" + channel + " :" + s);
            sout.Flush();
        }

        public void pure(string s)
        {
            Console.WriteLine("pure sent: " + s);
            sout.WriteLine(s);
            sout.Flush();
        }

        public void joinRoom(string s)
        {
            partRoom();
            this.channel = s;
            //inform irc about the channel you'd like to join.
            //sout.WriteLine("JOIN " + channel);
            sout.WriteLine("JOIN #" + channel);

            sout.Flush();
            inChannel = true;
        }

        public void joinRoom()
        {
            joinRoom(channel);
        }

        public void partRoom()
        {
            if (!inChannel)
                return;
            sout.WriteLine("PART #" + channel);
            inChannel = false;
           
        }


        #region private methods
        private void incomingChat(string s)
        {
            string[] split = s.Split(new char[] { ' ' });
            string text;
            string[] name;

            if(split[1] == "PRIVMSG" && split[2] == "#" + channel)
            {
                //we jolly well have a text.
                text = s.Remove(0, split[0].Length + split[1].Length + split[2].Length + 4); //4 is for 3 spaces parsed and the : just before the message.
                name = split[0].Split('!');
                name[0] = name[0].Remove(0, 1); // this removes the preceeding :.

                Console.WriteLine(name[0] + " : " + text);

                input.Enqueue(text);
            }

            
        }
        #endregion

    }
}
